import pygame,sys
from threading import Thread

pygame.init()
screen=pygame.display.set_mode((1040,400))
screen.fill('blue')
pygame.display.set_caption('PIANO')
main_key=pygame.Surface((20,200),pygame.HWSURFACE)
sub_key=pygame.Surface((18,140),pygame.HWSURFACE)
press=pygame.Surface((10,100), pygame.HWSURFACE)
lift=pygame.Surface((1040,100),pygame.HWSURFACE)
main_key.fill('white');sub_key.fill('black')
press.fill('yellow');lift.fill('blue')

def play(keys,i):
    pygame.mixer.music.load('source/'+keys[i]+'.mp3')
    pygame.mixer.music.play(start=0,fade_ms=500)
    position= origin + 5 + 20 * i
    if keys == blacks:
        position+=10
        if i > 1:
            position+=20
    screen.blit(press, (position, 220))

def run(sound):
    pygame.mixer.music.load('source/'+sound+'.mp3')
    pygame.mixer.music.play(start=0,fade_ms=500)

def put():
    global count
    count=(count+1)%3
    exec(f't{count}=Thread(target=target,args=args);t{count}.start()')

whites=('c-1', 'd-1', 'e-1', 'f-1', 'g-1', 'a-1', 'b-1')
blacks = ('c-#1', 'd-#1', 'f-#1', 'g-#1', 'a-#1')
low=('A2','A#2','B2')
origin=460
target=None
count=0

while True:
    pygame.draw.line(screen,'black',[0,200],[1040,200],1)
    for i in range(52):
        d=i*20
        screen.blit(main_key,(d,0))
        pygame.draw.line(screen,'black',[d,0],[d,200],1)
    for i in range(8):
        d=11+i*140
        screen.blit(sub_key,(d,0))
    for i in range(7):
        d0=51+i*140
        d1=71+i*140
        d2=111+i*140
        d3=131+i*140
        d4=151+i*140
        for j in range(5):
            screen.blit(sub_key,(eval('d'+str(j)),0))
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_z:
                whites=('C1', 'D1', 'E1', 'F1', 'G1', 'A1', 'B1')
                blacks = ('C#1', 'D#1', 'F#1', 'G#1', 'A#1')
                origin=40
            if event.key == pygame.K_x:
                whites=('C', 'D', 'E', 'F', 'G', 'A', 'B')
                blacks = ('C#', 'D#', 'F#', 'G#', 'A#')
                origin=180
            if event.key == pygame.K_c:
                whites=('c-', 'd-', 'e-', 'f-', 'g-', 'a-', 'b-')
                blacks = ('c-#', 'd-#', 'f-#', 'g-#', 'a-#')
                origin=320
            if event.key == pygame.K_v:
                whites=('c-1', 'd-1', 'e-1', 'f-1', 'g-1', 'a-1', 'b-1')
                blacks = ('c-#1', 'd-#1', 'f-#1', 'g-#1', 'a-#1')
                origin=460
            if event.key == pygame.K_b:
                whites = ('c-2', 'd-2', 'e-2', 'f-2', 'g-2', 'a-2', 'b-2')
                blacks = ('c-#2', 'd-#2', 'f-#2', 'g-#2', 'a-#2')
                origin=600
            if event.key == pygame.K_n:
                whites = ('c3', 'd3', 'e3', 'f3', 'g3', 'a3', 'b3')
                blacks = ('c-#3', 'd-#3', 'f-#3', 'g-#3', 'a-#3')
                origin=740
            if event.key == pygame.K_m:
                whites = ('c4', 'd4', 'e4', 'f4', 'g4', 'a4', 'b4')
                blacks = ('c-#4', 'd-#4', 'f-#4', 'g-#4', 'a-#4')
                origin=880
            for i in range(7):
                if event.key == 1073741913+i:
                    # play(whites,i)
                    target=play
                    args=(whites,i)
                    put()
                if event.key == 49+i and i < 5:
                    # play(blacks,i)
                    target=play
                    args=(blacks,i)
                    put()
                if event.key == 55+i and i < 3:
                    # run(low[i])
                    target=run
                    args=(low[i],)
                    put()
                    screen.blit(press,(5+10*i,220))
            if event.key == pygame.K_KP0:
                # run('c5')
                target=run
                args=('c5',)
                put()
                screen.blit(press,(1025,220))
        if event.type == pygame.KEYUP:
            screen.blit(lift,(0,220))
    pygame.display.flip()
