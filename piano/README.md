# 说明书

### 简介
这是一个模拟钢琴的Python小游戏脚本。

### 使用说明
1. 钢琴的**88**个键对应**88**个音，分为**7**个完整音组和**2**个不完整音组。 
2. 每个完整音组包含**7**个白键（自然音阶）和**5**个黑键（人造音阶）。 
3. 位于钢琴键盘**最左侧的不完整音组**从左到右依次对应电脑**大键盘**`7`、`8`、`9`。 
4. 位于钢琴键盘**最右侧的不完整音组**仅有**1**个白键，对应电脑**小键盘**`0`。 
5. **完整音组**中白键从左到右依次对应电脑**小键盘**`1`、`2`、`3`、`4`、`5`、`6`、`7`。 
6. **完整音组**中黑键从左到右依次对应电脑**大键盘**`1`、`2`、`3`、`4`、`5`。 
7. 游戏启动时**默认音组**为**小字一组**，位于钢琴键盘**正中间**，可用按键切换音组。 
8. 电脑键盘上`Z`、`X`、`C`、`V`、`B`、`N`、`M`从左到右依次对应钢琴键盘上**7**个完整音组。 
9. 按键时，钢琴键盘对应键位下方会有黄色高亮提示。

### 补充说明
1. 因个人兴趣编写，工期较短，有所不足见谅。 
2. 为了减少代码量而降低了代码的可读性。 
3. 关于技术交流与讨论，欢迎留言与联系。
