print('Converts a sequence file to FASTA format.')
seqfile=input('Enter the file name: ')
f=open(seqfile,'r')
seqs=iter(f)
g=open(seqfile+'.fa','w')
fasta=iter(g)
i=0
for line in seqs:
    i=i+1
    fasta.write('>Seq'+str(i)+'\n')
    fasta.write(line)
f.close()
g.close()
print('Converting over.')
