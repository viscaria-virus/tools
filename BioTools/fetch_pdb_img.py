# -*- coding : utf-8 -*-
import requests,random,time,sys,os,math,fake_useragent
f=iter(open('idlist','r'))
size=f.seek(0,2)
count=math.ceil(size/5)
f.seek(0,0)
prefix='https://cdn.rcsb.org/images/structures/'
suffix1='_model-1.jpeg'
suffix2='_assembly-1.jpeg'
suffix3='_models.jpeg'
header={'User-Agent':fake_useragent.UserAgent().random}
def attempt(url):
    global response,code
    response=requests.get(url,headers=header)
    code=response.status_code
    gap=random.randint(3,5)
    time.sleep(gap)
success=0;failure=0
sys.stdout.write('Downloading 0/'+str(count)+'\r')
while f.tell() < size:
    entry=f.read(5).replace('\n','').replace(',','').lower()
    link1=prefix+entry[1:3]+'/'+entry+'/'+entry+suffix1
    link2=prefix+entry[1:3]+'/'+entry+'/'+entry+suffix2
    link3=prefix+entry[1:3]+'/'+entry+'/'+entry+suffix3
    picture=entry+'.jpeg'
    if picture not in os.listdir():
        try:
            attempt(link1)
        except code != 200:
            attempt(link2)
        except code != 200:
            attempt(link3)
        except code != 200:
            h=iter(open('fail_list','a'))
            h.write(line)
            h.close()
            failure+=1
        finally:
            if code == 200:
                g=open(picture,'wb')
                g.write(response.content)
                g.close()
                success+=1
    else:
        success+=1
    rate=str(success+failure)+'/'+str(count)
    sys.stdout.write('Downloading'+rate+'\r')
f.close()
print('Download Over\n')
print('Success = ',success)
print('Failure = ',failure)
