#!/bin/bash
$method --quiet --thread -1 "$input" > "$output" &&
if [ $input == "alns.fa" ]
then
	rm $input
	rm subMSAtable
fi
echo -e "\033[32m Alignment finish \033[0m"
