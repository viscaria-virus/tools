import sys
from sequence import Nucleotide as nucl


def receive(i):
    global SeqTp, SeqN, LR
    if sys.argv[i] == '-t':
        SeqTp = sys.argv[i + 1]
    if sys.argv[i] == '-n':
        SeqN = int(sys.argv[i + 1])
    if sys.argv[i] == '-l':
        MinL = eval(sys.argv[i + 1])[0]
        MaxL = eval(sys.argv[i + 1])[1]
        LR = range(MinL, MaxL)


def entry():
    global SeqTp, SeqN, LR
    SeqTp = input('Generate DNA or RNA?\nYour choice: ')
    SeqN = int(input('Enter the number of the random sequences to be generated: '))
    print('Decide the length range of the randomly generated sequences.')
    lr = eval(input('Input the length range(two integers seperated by ","): '))
    MinL = lr[0]
    MaxL = lr[1]
    LR = range(MinL, MaxL)


if __name__ == '__main__':
    p = 1
    while p <= len(sys.argv) - 1:
        receive(p)
        p += 2
    if ('SeqN' in globals()) and ('LR' in globals()):
        if 'SeqTp' not in globals():
            SeqTp = 'DNA'
    else:
        entry()
    f = iter(open('Seqs.fa', 'w'))
    for j in range(1, SeqN + 1):
        f.write('>Seq' + str(j) + '\n')
        seq = str(nucl.random(LR, SeqTp))
        f.write(seq + '\n')
    f.close()
    print('Generation Over')
