import random


class Nucleotide(str):
    '''A class of Nucleic Acid Sequences.'''

    def __init__(self, seq='', name='', type=''):
        '''The initialization method to generate an object of the class,
        and automatically generates its attributes.'''
        if not seq.isalpha() and seq != '':
            raise ValueError
        for base in seq.upper():
            if base not in 'ATCGU':
                raise ValueError
        self.sequence = seq.upper()
        str.__init__(self.sequence)
        self.type = type
        self.__check()
        self.name = name

    def __add__(self, other):
        '''A magic method to allow connecting sequences.'''
        result = Nucleotide(self.sequence + Nucleotide(other).sequence)
        return result

    def __check(self):
        '''A built-in-method of the class to check the type of the sequence,
        and automatically correct the wrong type entries.'''
        baseT = self.count('T')
        baseU = self.count('U')
        if baseT > 0 and baseU == 0:
            self.type = 'DNA'
        elif baseU > 0 and baseT == 0:
            self.type = 'RNA'
        elif baseU > 0 and baseT > 0:
            self.type = 'HYBRID'
        elif self.type not in ('DNA', 'RNA'):
            self.type = 'UNKNOWN'

    @classmethod
    def random(cls, length, seqtype=random.choice(['DNA', 'RNA', 'HYBRID'])):
        '''A class method to randomly generate a sequence,
        the sequence length must be specified by an integer or a range,
        while its chemical type can be specified or randomly generated.'''
        seq = Nucleotide()
        seq.type = seqtype.upper()
        bases = ['A', 'C', 'G', 'T', 'U']
        if seq.type == 'DNA':
            bases.remove('U')
        elif seq.type == 'RNA':
            bases.remove('T')
        elif seq.type != 'HYBRID':
            raise TypeError
        if type(length) == range:
            length = random.choice(length)
        for i in range(length):
            seq += random.choice(bases)
        return cls(seq)

    def naming(self, value):
        '''A method to name the sequence.'''
        self.name = value

    def reverse(self):
        '''A method to generate the reverse chain of a sequence.'''
        chain = list(self)
        chain.reverse()
        seq = ''
        for base in chain:
            seq += base
        self.reverse_chain = Nucleotide(seq)
        self.reverse_chain.type = self.type
        return self.reverse_chain

    def convert(self):
        '''A method to convert a sequence from DNA to RNA or vice versa.'''
        if self.type == 'DNA':
            chain = self.replace('T', 'U')
            seq = Nucleotide(chain)
            seq.type = 'RNA'
        elif self.type == 'RNA':
            chain = self.replace('U', 'T')
            seq = Nucleotide(chain)
            seq.type = 'DNA'
        else:
            raise TypeError
        return seq

    def complement(self):
        '''A method to generate the complementary chain of a sequence.'''
        seq = ''
        for base in self.sequence:
            if base == 'C':
                seq += 'G'
            elif base == 'G':
                seq += 'C'
            elif base in 'TU':
                seq += 'A'
            elif base == 'A':
                if self.type == 'DNA':
                    seq += 'T'
                elif self.type == 'RNA':
                    seq += 'U'
                elif self.type in ['HYBRID', 'UNKNOWN']:
                    raise TypeError
        self.complementary_chain = Nucleotide(seq)
        self.complementary_chain.type = self.type
        return self.complementary_chain

    def transcribe(self, start=0, end=0):
        '''A method to generate the transcript of a sequence.'''
        if end == 0:
            end = len(self.sequence)
        if self.type == 'DNA':
            transcript = self.convert().complement()[start:end]
            self.transcript = Nucleotide(transcript)
            self.transcript.type = 'RNA'
            self.mRNA = self.transcript
        if self.type == 'RNA':
            transcript = self.convert().complement()[start:end]
            self.transcript = Nucleotide(transcript)
            self.transcript.type = 'DNA'
            self.cDNA = self.transcript
        return self.transcript

    def shuffle(self, mode='SN'):
        '''A method to shuffle a sequence.'''
        result = ''
        if mode == 'SN':
            bases = list(self)
        if mode == 'Gis':
            bases = list(self.replace('GGG', '3').replace('GG', '2'))
        random.shuffle(bases)
        for base in bases:
            result += base
        randseq = Nucleotide(result.replace('3', 'GGG').replace('2', 'GG'))
        return randseq

    def fasta(self):
        '''A method to generate the FASTA format of the sequence.'''
        self.fa = '>' + self.name + '\n' + self.sequence + '\n'
        print(self.fa)
        return self.fa
