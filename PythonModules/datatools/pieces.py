import os
from hashlib import md5


def make_md5(src):
    '''A function to generate the MD5 code of a file.'''
    f = open(src, 'rb')
    md5_code = md5(f.read()).hexdigest()
    f.close()
    g = open(src + '.md5', 'w')
    g.write(md5_code + '\n')
    g.close()


def split(src):
    '''A function to divide a file into small parts.'''
    size = os.path.getsize(src)
    f = iter(open(src, 'rb'))
    suffix = src.split('.')[-1]
    prefix = src.replace('.' + suffix, '')
    i = 1
    parts = prefix + '_parts_' + suffix
    os.mkdir(parts)
    while f.tell() < size:
        part = parts + '/' + prefix + '_' + str(i)
        g = open(part, 'wb')
        g.write(f.read(8000))
        g.close()
        i += 1
    f.close()
    make_md5(src)
    os.rename(src + '.md5', parts + '/' + src + '.md5')


def check(file, verify):
    '''A function to check the integrity of a file.'''
    f = open(file, 'rb')
    md5_code = md5(f.read()).hexdigest()
    f.close()
    g = open(verify, 'r')
    md5_src = g.read().replace('\n', '')
    g.close()
    return md5_code == md5_src


def combine(srcs):
    '''A function to recombine parts into a complete file.'''
    parts = os.listdir(srcs)
    tgf = srcs.replace('_parts_', '.')
    f = iter(open(tgf, 'wb'))
    prefix = tgf.split('.')[0]
    for i in range(1, len(parts)):
        part = prefix + '_' + str(i)
        g = open(srcs + '/' + part, 'rb')
        f.write(g.read())
        g.close()
    f.close()
