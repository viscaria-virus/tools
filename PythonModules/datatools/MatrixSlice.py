def submatrix(matrix, left=0, right=0, up=0, down=0):
    if right == 0:
        rows = matrix[left:]
    else:
        rows = matrix[left:right]
    if down == 0:
        cols = map(lambda x: x[up:], rows)
    else:
        cols = map(lambda x: x[up:down], rows)
    return list(cols)


# import random
#
# array = []
# for i in range(10):
#     rows = [random.randrange(21) for j in range(10)]
#     array.append(rows)
# print(array)
# print(submatrix(array, 2, 4, 1, 3))
