def search(jsonstr, path):
    '''A function to query a specified content
    through its path in a json string.'''
    keys = path.split('/')
    src = jsonstr
    for i in range(len(keys)):
        key = keys[i]
        if key == '':
            continue
        if isinstance(src, dict):
            value = src[key]
        if isinstance(src, list):
            value = src[int(key)]
        src = value
    return value
