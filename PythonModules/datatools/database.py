from pymongo import MongoClient as _mongo
from redis import Redis as _redis
from pymysql import connect as _mysql
from os import path as _path
from getpass import getpass as _getpass


class database:
    '''An ancestral class defining some basic methods of the database management.'''

    def link(self, system, host, port, user='', password=''):
        '''Connect to a specified database server,
        supporting several type of systems,
        including MongoDB,Redis,MySQL/MariaDB.'''
        self._system = system
        self._host = host
        self._port = port
        if self._system == 'redis':
            self._client = _redis(host=self._host, port=self._port, decode_responses=True)
        if self._system == 'mongo':
            self._client = _mongo(host=self._host, port=self._port)
        if self._system == 'mysql':
            self._user = user
            self.__password = password
            self._client = _mysql(host=self._host, port=self._port, charset='utf8', user=self._user,
                                  password=self.__password)
            self._cursor = self._client.cursor()
        self.address = self._system + '://' + self._host + ':' + str(self._port)
        self._path = self.address

    def __init__(self, system, host, port, user='', password=''):
        '''Initialize a database connection object to operate on database management.'''
        database.link(self, system, host, port, user, password)

    def open(self):
        '''Restart the last connection.'''
        database.link(self, self._system, self._host, self._port)

    def close(self):
        '''Close the current database connection.'''
        self._path = None
        self._client.close()
        self._client = None

    def cwd(self):
        '''Get the current working directory.'''
        return self._path


class mongo(database):
    '''A class providing methods to operate on MongoDB.'''

    def __init__(self, host='localhost', port=27017):
        '''Initialize a MongoDB connection.'''
        database.__init__(self, 'mongo', host, port)

    def link(self, host, port):
        '''Connect to a MongoDB server.'''
        database.link(self, 'mongo', host, port)

    def go2db(self, dbname):
        '''Open a database on the MongoDB server.'''
        self._dbname = dbname
        self._db = self._client[self._dbname]
        self._path = self.address + '/' + self._dbname
        return self.cwd()

    def go2set(self, setname):
        '''Open a dataset in [the current database]/[another database on the server].'''
        if '/' in setname:
            dbname = _path.dirname(setname)
            self.go2db(dbname)
            self._setname = _path.basename(setname)
        else:
            self._setname = setname
        self._ds = self._db[self._setname]
        self._path = self.address + '/' + self._dbname + '/' + self._setname
        return self.cwd()

    def deldb(self, dbname):
        '''Delete a database on the server.'''
        self._client.drop_database(dbname)
        if dbname == self._dbname:
            self._path = self.address
            self._db = None
            self._ds = None

    def delset(self, setname):
        '''Delete a dataset in [the current database]/[another database on the server].'''
        self._db.drop_collection(setname)
        if setname == self._setname:
            self._path = self.address + '/' + self._dbname
            self._ds = None

    def browse(self):
        '''Browse the data in the current dataset.'''
        for data in self._ds.find():
            print(data)

    def insert(self, data):
        '''Insert a piece of data or several pieces of data into the current dataset.'''
        tip = 'Required positional argument should be a dict or a list contains dicts'
        if isinstance(data, dict):
            self._ds.insert_one(data)
        if isinstance(data, list):
            for i in range(len(data)):
                if not isinstance(data[i], dict):
                    raise TypeError(tip)
            self._ds.insert_many(data)
        elif 'data' in vars():
            raise TypeError(tip)

    def search(self, **query):
        '''Search for a piece of matched data in the current dataset.'''
        return self._ds.find_one(query)

    def modify(self, entry, n=0, **item):
        '''Modify specified items of all matched data in the current dataset.'''
        change = {'$set': item}
        if n == 0:
            self._ds.update_many(entry, change)
        else:
            for i in range(n):
                self._ds.update_one(entry, change)

    def listdb(self):
        '''List all database names on the server. '''
        return self._client.list_database_names()

    def listset(self, *dbname):
        '''List all dataset names in [the current database]/[another database on the server].'''
        if len(dbname) != 0:
            self.go2db(*dbname)
        return self._db.list_collection_names()

    def delete(self, **entry):
        '''Delete all matched data in the current dataset.'''
        self._ds.delete_many(entry)

    def clear(self):
        '''Delete all data in the current dataset.'''
        self._ds.delete_many({})


class redis(database):
    '''A class providing methods to operate on Redis.'''

    def __init__(self, host='localhost', port=6379):
        '''Initialize a Redis connection.'''
        database.__init__(self, 'redis', host, port)
        self._path = self.address + '/' + '0'

    def link(self, host, port):
        '''Connect to a Redis server.'''
        database.link(self, 'redis', host, port)

    def go2db(self, n):
        '''Open a database on the Redis server.'''
        self._client.select(n)
        self._path = self.address + '/' + str(n)
        return self.cwd()

    def list(self):
        '''Show all keys in the current database.'''
        return self._client.keys()

    def clear(self):
        '''Delete all data in the current database.'''
        self._client.flushdb()

    def set(self, **data):
        '''Insert several key-value pairs into the current database;
        if a key exists, its value will be covered;
        a value can be a string/digit/list/set/dict.'''
        for key in data:
            self._client.delete(key)
        for key in data:
            value = data[key]
            if isinstance(value, list):
                for e in value:
                    self._client.lpush(key, e)
            elif isinstance(value, set):
                for e in value:
                    self._client.sadd(key, e)
            elif isinstance(value, dict):
                for item in value:
                    self._client.hset(key, item, value[item])
            else:
                self._client.set(key, value)

    def search(self, key):
        '''Get the value of the specified key.'''
        if self._client.type(key) == 'string':
            return self._client.get(key)
        if self._client.type(key) == 'list':
            length = self._client.llen(key)
            return self._client.lrange(key, 0, length)
        if self._client.type(key) == 'set':
            return self._client.smembers(key)
        if self._client.type(key) == 'hash':
            return self._client.hgetall(key)

    def browse(self):
        '''Browse the data in the current database.'''
        for key in self._client.keys():
            print(key, ':', self.search(key))

    def modify(self, key, *n, **change):
        '''Modify the specified part of a string/list/dict in the current database.'''
        if self._client.type(key) == 'string':
            self._client.setrange(key, *n, **change)
        if self._client.type(key) == 'list':
            self._client.lset(key, *n, **change)
        if self._client.type(key) == 'hash':
            for item in change:
                self._client.hset(key, item, change[item])

    def remove(self, key, data, n=1):
        '''Remove elements in a list/set/dict in the current database.'''
        if self._client.type(key) == 'list':
            self._client.lrem(key, n, data)
        if self._client.type(key) == 'set':
            self._client.srem(key, data)
        if self._client.type(key) == 'hash':
            self._client.hdel(key, data)

    def insert(self, key, data, *n):
        '''Insert elements into a list/set/dict in the current database.'''
        if self._client.type(key) == 'list':
            reference = self._client.lindex(key, *n)
            self._client.linsert(key, 'before', reference, data)
        if self._client.type(key) == 'set':
            self._client.sadd(key, data)
        if self._client.type(key) == 'hash':
            self._client.hset(key, *n, data)

    def rename(self, old, new):
        '''Rename a key in the current database.'''
        self._client.rename(old, new)

    def close(self):
        '''Close the current database connection.'''
        self._client.save()
        database.close(self)

    def delete(self, *keys):
        '''Delete several key-value pairs in the current database.'''
        for key in keys:
            self._client.delete(key)


class mysql(database):
    '''A class providing methods to operate on MySQL/MariaDB.'''

    def __init__(self, host='localhost', port=3306, user='', password=''):
        '''Initialize a MySQL/MariaDB connection.'''
        if user != '' and password == '':
            password = _getpass('Enter the password: ')
        database.__init__(self, 'mysql', host, port, user, password)
        login = 'Connect as user: ' + user
        print(login)

    def link(self, host, port, user, password):
        '''Connect to a MySQL/MariaDB server.'''
        if user != '' and password == '':
            password = _getpass('Enter the password: ')
        database.link(self, 'mysql', host, port, user, password)
        login = 'Connect as user: ' + user
        print(login)

    def open(self):
        '''Restart the last connection.'''
        database.link(self, self._system, self._host, self._port, self._user, self.__password)

    def close(self):
        '''Close the current database connection.'''
        self._client.commit()
        database.close(self)

    def exec_sql(self, sql):
        '''Execute an SQL sentence on the server.'''
        self._cursor.execute(sql)
        return self._cursor.fetchall()

    def listdb(self):
        '''List all database names on the server.'''
        result = self.exec_sql('SHOW DATABASES')
        dbs = []
        for db in result:
            dbs.append(db[0])
        return dbs

    def go2db(self, dbname):
        '''Open a database on the MySQL/MariaDB server.'''
        self._db = dbname
        self._client.select_db(self._db)
        self._path = self.address + '/' + self._db
        return self.cwd()

    def listtab(self):
        '''List all table names in the current database.'''
        result = self.exec_sql('SHOW TABLES')
        tabs = []
        for tab in result:
            tabs.append(tab[0])
        return tabs

    def go2tab(self, tabname):
        '''Open a table in [the current database]/[another database on the server].'''
        if '/' in tabname:
            dbname = _path.dirname(tabname)
            self.go2db(dbname)
            self._tab = _path.basename(tabname)
        else:
            self._tab = tabname
        if self._tab not in self.listtab():
            raise ValueError('Table not found in specified database.')
        self._path = self.address + '/' + self._db + '/' + self._tab
        return self.cwd()

    def format(self, tab=''):
        '''Get the structure of [the current table]/[another table in the current database.'''
        if tab == '':
            tab = self._tab
        result = self.exec_sql('DESC ' + tab)
        tabform = {}
        for item in result:
            tabform[item[0]] = item[1]
        return tabform

    def browse(self):
        '''Browse the data in the current table.'''
        format = self.exec_sql('DESC ' + self._tab)
        query = ''
        for item in format:
            query += item[0] + ','
        items = query[:-1].split(',')
        result = self.exec_sql('SELECT ' + query[:-1] + ' FROM ' + self._tab)
        for data in result:
            record = {}
            i = 0
            for item in data:
                record[items[i]] = item
                i += 1
            print(record)

    def search(self, query, items='*'):
        '''Search for all matched data in the current table.'''
        if items == '*':
            form = self.exec_sql('DESC ' + self._tab)
            items = ''
            for item in form:
                items += item[0] + ','
            items = items[:-1]
        result = self.exec_sql('SELECT ' + items + ' FROM ' + self._tab + ' WHERE ' + query)
        records = []
        items = items.split(',')
        for data in result:
            record = {}
            i = 0
            for item in data:
                record[items[i]] = item
                i += 1
            records.append(record)
        return records

    def insert(self, **data):
        '''Insert a piece of data or several pieces of data into the current table.'''
        keys = tuple(data.keys())
        values = tuple(data.values())
        items = ''
        contents = ''
        for key in keys:
            items += key + ','
        items = '(' + items[:-1] + ')'
        for value in values:
            if isinstance(value, str):
                contents += "'" + value + "'" + ','
            else:
                contents += str(value) + ','
        contents = '(' + contents[:-1] + ')'
        return self.exec_sql('INSERT INTO ' + self._tab + items + 'VALUES' + contents)

    def modify(self, entry, data):
        '''Modify all matched data in the current table.'''
        return self.exec_sql('UPDATE ' + self._tab + ' SET ' + data + ' WHERE ' + entry)

    def delete(self, entry):
        '''Delete all matched data in the current table.'''
        return self.exec_sql('DELETE FROM ' + self._tab + ' WHERE ' + entry)

    def clear(self):
        '''Delete all data in the current table.'''
        try:
            return self.exec_sql('TRUNCATE TABLE' + self._tab)
        except Exception:
            return self.exec_sql('DELETE FROM ' + self._tab)

    def copy(self, tab1, tab2):
        '''Copy an existing table to a new table in the current database.'''
        start = self.exec_sql('CREATE TABLE ' + tab2 + ' LIKE ' + tab1)
        end = self.exec_sql('INSERT INTO ' + tab2 + ' SELECT * FROM ' + tab1)
        result = str(start) + '-' + str(end)
        return result

    def mkdb(self, dbname):
        '''Create a new database on the server.'''
        return self.exec_sql('CREATE DATABASE ' + dbname + ' CHARACTER SET utf8')

    def mktab(self, tabname, **tabform):
        '''Create a new table in the current database according to a specified structure.'''
        contents = ''
        for item in tabform:
            content = tabform[item]
            contents += item + ' ' + content + ','
        contents = '(' + contents[:-1] + ')'
        return self.exec_sql('CREATE TABLE ' + tabname + contents)

    def imitate(self, tab1, tab2):
        '''Create a new table with the same structure as an existing table.'''
        return self.exec_sql('CREATE TABLE ' + tab1 + ' LIKE ' + tab2)

    def deldb(self, dbname):
        '''Delete a database on the server.'''
        if dbname == self._db:
            del self._db, self._tab
            self._path = self.address
        return self.exec_sql('DROP DATABASE ' + dbname)

    def deltab(self, tabname):
        '''Delete a table in the current database.'''
        if tabname == self._tab:
            del self._tab
            self.go2db(self._db)
        return self.exec_sql('DROP TABLE ' + tabname)

    def run_script(self, script):
        '''Run an SQL script on the server.'''
        if script.endswith('.sql'):
            f = iter(open(script, 'r'))
            sql = ''
            while f.tell() < _path.getsize(script):
                sql += f.read(1)
                if sql.endswith(';'):
                    print(self.exec_sql(sql))
                    sql = ''
            f.close()
        else:
            raise TypeError('File type identification error.')
