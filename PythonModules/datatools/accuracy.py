def exact(x, y, operation):
    x = str(float(x))
    y = str(float(y))
    m = len(x.split('.')[1])
    n = len(y.split('.')[1])
    multiple = max(m, n)
    X = int(x.replace('.', '') + '0' * (multiple - m))
    Y = int(y.replace('.', '') + '0' * (multiple - n))
    result = eval(f'{X}{operation}{Y}')
    if operation == '*':
        result /= 100 ** multiple
    if operation in ('+', '-'):
        result /= 10 ** multiple
    return result


def convert(expression):
    expression = expression.replace(' ', '')
    n = len(expression)
    array = list(expression)
    for i in range(n):
        if expression[i:i + 2] == '(-':
            p = expression[i + 2:].index(')') + i + 2
            array[p] = ''
    return ''.join(array).replace('(-', '_')


def calculate(expression):
    array = convert(expression)
    if '(' in array:
        head = array.find('(')
        tail = array.rfind(')')
        subresult = calculate(array[head + 1:tail])
        array = array[:head] + str(subresult) + array[tail + 1:]
    if array[0] == '-':
        array = '_' + array[1:]
    expression = array
    symbols = '+-*/'
    for symbol in symbols:
        array = array.replace(symbol, '\t')
    data = array.replace('_', '-').split('\t')
    operations = []
    mul_div = []
    i = 0
    for character in expression:
        if character in symbols:
            operations.append(character)
            if character in '*/':
                mul_div.append(i)
            i += 1
    for i in mul_div:
        data[i + 1] = str(exact(data[i], data[i + 1], operations[i]))
    for i in mul_div[::-1]:
        operations.pop(i)
        data.pop(i)
    for operation in operations:
        data[1] = str(exact(data[0], data[1], operation))
        del data[0]
    return eval(data[0])

# import time,random
# data=[ random.uniform(1,5).__format__('.3f') for i in range(100) ]
# operations=[ random.choice('+-*/') for i in range(99) ]
# expression=''
# for i in range(99):
#     expression+=data[i]+operations[i]
# expression+=data[-1]
# start=time.time()
# result1=calculate(expression)
# mid=time.time()
# result2=eval(expression)
# end=time.time()
# print(result1,mid-start)
# print(result2,end-mid)
