import schedule, sys, os, shutil, time
from datetime import datetime
from SCIscan.integrate import integrate, export

if __name__ == '__main__':
    receiver = input('Enter an email address for receiving the results: ')
    project = sys.argv[1]
    tasks = os.listdir(project)
    os.chdir(project)
    os.mkdir('history')
    with open(tasks[0], 'r') as f:
        timeset = f.readline()
        period = timeset[1 + timeset.index('{'):timeset.index('}')]
        constant = timeset[1 + timeset.index('('):timeset.index(')')]
        day = timeset[1 + timeset.index('['):timeset.index(']')]


    def job():
        os.chdir(project)
        moment = datetime.now().strftime('%Y%m%d')
        for task in tasks:
            if task.endswith('.py'):
                os.system('python ' + task)
                time.sleep(120)
                results = integrate('..')
                out = task[:-3] + '_result_' + moment
                export(results, out)
                history = task[:-3] + '_' + moment
                os.mkdir(history)
                for file in os.listdir('..'):
                    if file.split('.')[-1] in ('txt', 'csv', 'xls'):
                        shutil.move('../' + file, history)
        log = 'log_' + moment + '.txt'
        os.rename('log.txt', log)
        for file in os.listdir():
            if not file.endswith('.py'):
                shutil.move(file, 'history')


    def check():
        date = datetime.now().day
        if date == day:
            job()


    if period == 'daily':
        schedule.every().day.at(constant).do(job)
    if period == 'weekly':
        if day == str(1):
            schedule.every().monday.at(constant).do(job)
        if day == str(2):
            schedule.every().tuesday.at(constant).do(job)
        if day == str(3):
            schedule.every().wednesday.at(constant).do(job)
        if day == str(4):
            schedule.every().thursday.at(constant).do(job)
        if day == str(5):
            schedule.every().friday.at(constant).do(job)
        if day == str(6):
            schedule.every().saturday.at(constant).do(job)
        if day == str(7):
            schedule.every().sunday.at(constant).do(job)
    if period == 'monthly':
        schedule.every().day.at(constant).do(check)
    print('Task start...')
    while True:
        schedule.run_pending()
