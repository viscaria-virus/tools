from SCIscan.sciscan.retreat import extract_doi, filter_doi


def link(storage):
    results = []
    dois0 = extract_doi('ScienceDirect', storage)
    dois1 = extract_doi('Wiley', storage)
    dois2 = extract_doi('PubMed', storage)
    dois3 = extract_doi('Springer', storage)
    dois4 = extract_doi('Taylor', storage)
    dois5 = extract_doi('WOS', storage)
    total = []
    for i in range(6):
        total += eval('dois' + str(i))
    unique = filter_doi(total)
    for doi in unique:
        result = doi + '<<SciHub'
        results.append(result)
    return results


if __name__ == '__main__':
    source = input('Enter the results storage path: ')
    out = input('Enter the output file name: ')
    links = link(source)
    with iter(open(out, 'w')) as f:
        for link in links:
            f.write(link + '\n')
    print('Downloading Configuration Generated.')
