from SCIscan.sciscan.downloader import Task


def batch_download():
    driver = input('Enter a webdriver path: ')
    config = input('Enter a list file name: ')
    storage = input('Enter the storage path：')
    task = Task(driver, storage)
    task.get_docs(config)


if __name__ == '__main__':
    batch_download()
    print('Downloading Complete.')
