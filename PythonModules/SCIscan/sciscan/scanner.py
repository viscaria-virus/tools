import random, time
from urllib import parse
from selenium import webdriver
from selenium.webdriver.edge.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from collections import OrderedDict


class Project:
    def create(self, storage='', bgt=False, tool='Edge'):
        self.bgt = bgt
        self.tool = tool.title()
        if self.tool == 'Edge':
            options = webdriver.EdgeOptions()
        if self.tool == 'Chrome':
            options = webdriver.ChromeOptions()
        if self.tool == 'Firefox':
            options = webdriver.FirefoxOptions()
        options.add_argument('--window-size=1000,600')
        if self.bgt == True:
            options.add_argument('--headless')
        if storage != '':
            path = {'download.default_directory': storage}
            options.add_experimental_option('prefs', path)
        self.options = options

    def __init__(self, storage='', bgt=False, tool='Edge'):
        Project.create(self, storage, bgt, tool)

    def start(self, driver):
        Project.driver = driver
        service = Service(Project.driver)
        if self.tool == 'Edge':
            Project.browser = webdriver.Edge(service=service, options=self.options)
        if self.tool == 'Chrome':
            Project.browser = webdriver.Chrome(service=service, options=self.options)
        if self.tool == 'Firefox':
            Project.browser = webdriver.Firefox(service=service, options=self.options)
        if self.bgt == False:
            Project.browser.minimize_window()

    def close(self):
        self.browser.close()


class WOS(Project):
    def __init__(self, storage='', bgt=True):
        Project.__init__(self, storage, bgt)
        self.point = 0

    def login(self, user, key):
        self.username = user
        self.__password = '[' + key + ']'
        suffix = dict(app='wos', loginId=self.username)
        tail = parse.urlencode(suffix)
        self.url = f'https://access.clarivate.com/login?{tail}'
        self.browser.get(self.url)
        self.browser.implicitly_wait(5)
        time.sleep(1 + random.random())
        box = self.browser.find_element(by=By.XPATH, value='//*[@id="mat-input-1"]')
        box.send_keys(self.__password)
        time.sleep(1 + random.random())
        button = self.browser.find_element(by=By.XPATH, value='//*[@id="signIn-btn"]')
        button.click()
        time.sleep(10)
        try:
            accept = self.browser.find_element(by=By.XPATH, value='//*[@id="onetrust-accept-btn-handler"]')
            accept.click()
            time.sleep(1 + random.random())
        except Exception:
            pass

    def add_query(self, **entry):
        entry = OrderedDict(entry)
        items = (
        'subject', 'title', 'author', 'publication', 'year', 'date', 'abstract', 'address', 'authorID', 'DOI', 'editor',
        'group')
        if not set(entry.keys()).issubset(set(items)):
            raise ValueError('Unrecognized entry in query')
        else:
            add_line = self.browser.find_element(by=By.XPATH, value='//button[@aria-label="添加行"]')
        for item in entry:
            if self.point > 2:
                add_line.click()
                time.sleep(1 + random.random())
            menu = self.browser.find_elements(by=By.XPATH, value='//app-select-search-field//button')[self.point]
            menu.click()
            time.sleep(1 + random.random())
            choice = self.browser.find_elements(by=By.XPATH, value='//div[@role="option"]')[items.index(item)]
            choice.click()
            time.sleep(1 + random.random())
            box = self.browser.find_elements(by=By.XPATH, value='//input[@aria-label="检索框"]')[self.point]
            box.send_keys(entry[item])
            time.sleep(1 + random.random())
            self.point += 1

    def query(self):
        search = self.browser.find_element(by=By.XPATH, value='//button[@data-ta="run-search"]')
        search.click()
        self.browser.implicitly_wait(5)
        time.sleep(1 + random.random())
        export = self.browser.find_element(by=By.XPATH, value='//app-export-menu//button')
        export.click()
        time.sleep(1 + random.random())
        format = self.browser.find_element(by=By.XPATH, value='//*[@id="exportToExcelButton"]')
        format.click()
        time.sleep(1 + random.random())
        option = self.browser.find_element(by=By.XPATH, value='//*[@id="radio3"]')
        option.click()
        time.sleep(1 + random.random())
        record = self.browser.find_element(by=By.XPATH, value='//button[@class="dropdown"]')
        record.click()
        time.sleep(1 + random.random())
        entry = self.browser.find_elements(by=By.XPATH, value='//div[@role="option"]')[1]
        entry.click()
        time.sleep(1 + random.random())
        output = self.browser.find_element(by=By.XPATH, value='//button[@color="primary"]')
        output.click()
        time.sleep(5)

    def reset(self):
        self.browser.get('https://www.webofscience.com/wos/alldb/basic-search')
        self.browser.implicitly_wait(5)
        time.sleep(1 + random.random())
        del_line = self.browser.find_element(by=By.XPATH, value='//button[@data-ta="remove-search-row"]')
        for i in range(3, self.point):
            del_line.click()
            time.sleep(1 + random.random())
        try:
            clears = self.browser.find_elements(by=By.XPATH, value='//button[@aria-label="从此行中清除检索词"]')
            for clear in clears:
                clear.click()
                time.sleep(1 + random.random())
        except Exception:
            pass
        self.point = 0


class PubMed(Project):
    def __init__(self, storage='', bgt=True):
        Project.__init__(self, storage, bgt)

    def login(self):
        self.url = 'https://pubmed.ncbi.nlm.nih.gov/advanced/'
        self.browser.get(self.url)
        self.browser.implicitly_wait(5)
        time.sleep(1 + random.random())
        self.entry = ''

    def add_query(self, **entry):
        entry = OrderedDict(entry)
        items = (
        'All', 'Affiliation', 'Author', 'Book', 'Editor', 'Investigator', 'Issue', 'Journal', 'Publisher', 'Text Word',
        'Title', 'Title/Abstract')
        if not set(entry.keys()).issubset(set(items)):
            raise ValueError('Unrecognized entry in query')
        for item in entry:
            if item == 'All':
                content = '(' + entry[item] + ')'
            else:
                content = '(' + entry[item] + '[' + item + '])'
            if self.entry != '':
                content = ' AND ' + content
            self.entry += content
            self.entry = '(' + self.entry + ')'

    def query(self):
        box = self.browser.find_element(by=By.XPATH, value='//*[@id="query-box-input"]')
        box.send_keys(self.entry)
        time.sleep(1 + random.random())
        search = self.browser.find_element(by=By.XPATH, value='//button[@class="search-btn"]')
        search.click()
        self.browser.implicitly_wait(5)
        time.sleep(1 + random.random())
        try:
            save = self.browser.find_element(by=By.XPATH, value='//*[@id="save-results-panel-trigger"]')
            save.click()
            time.sleep(1 + random.random())
            selection = self.browser.find_element(by=By.XPATH, value='//*[@id="save-action-selection"]')
            Select(selection).select_by_value('all-results')
            time.sleep(1 + random.random())
            format = self.browser.find_element(by=By.XPATH, value='//*[@id="save-action-format"]')
            Select(format).select_by_value('pubmed-text')
            time.sleep(1 + random.random())
            export = self.browser.find_element(by=By.XPATH, value='//*[@id="save-action-panel-form"]/div[3]/button[1]')
            export.click()
        except Exception:
            print('Search no result.')
        time.sleep(5)

    def reset(self):
        self.login()


class ScienceDirect(Project):
    def __init__(self, storage='', bgt=True):
        Project.__init__(self, storage, bgt)

    def login(self):
        self.url = 'https://www.sciencedirect.com/search'
        self.browser.get(self.url)
        time.sleep(10)
        try:
            later = self.browser.find_element(by=By.XPATH, value='//*[@id="pendo-button-1a341f68"]')
            later.click()
            time.sleep(1 + random.random())
        except Exception:
            pass
        show = self.browser.find_element(by=By.XPATH, value='//span[text()="Show all fields"]')
        show.click()
        time.sleep(1 + random.random())

    def set_item(self, entry, item=''):
        items = ('journal', 'book', 'year', 'author', 'affiliation', 'volume', 'issue', 'page', 'keywords', 'title',
                 'references', 'ISSN', 'ISBN')
        if item == '':
            box = self.browser.find_element(by=By.XPATH, value='//*[@id="qs"]')
        elif item not in items:
            raise ValueError('Unrecognized entry in query')
        else:
            n = items.index(item)
            if n <= 1:
                box = self.browser.find_element(by=By.XPATH, value='//*[@id="pub"]')
            elif n >= 11:
                box = self.browser.find_element(by=By.XPATH, value='//*[@id="docId"]')
            elif n == 2:
                box = self.browser.find_element(by=By.XPATH, value='//*[@id="date"]')
            elif n == 3:
                box = self.browser.find_element(by=By.XPATH, value='//*[@id="authors"]')
            elif n == 4:
                box = self.browser.find_element(by=By.XPATH, value='//*[@id="affiliations"]')
            elif n == 5:
                box = self.browser.find_element(by=By.XPATH, value='//*[@id="volume"]')
            elif n == 6:
                box = self.browser.find_element(by=By.XPATH, value='//*[@id="issue"]')
            elif n == 7:
                box = self.browser.find_element(by=By.XPATH, value='//*[@id="page"]')
            elif n == 8:
                box = self.browser.find_element(by=By.XPATH, value='//*[@id="tak"]')
            elif n == 9:
                box = self.browser.find_element(by=By.XPATH, value='//*[@id="title"]')
            else:
                box = self.browser.find_element(by=By.XPATH, value='//*[@id="references"]')
        box.send_keys(entry)
        time.sleep(1 + random.random())

    def query(self):
        search = self.browser.find_element(by=By.XPATH, value='//button[@aira-label="Submit advanced search"]')
        search.click()
        self.browser.implicitly_wait(5)
        time.sleep(1 + random.random())
        choose = self.browser.find_element(by=By.XPATH, value='//*[@id="select-all-results"]')
        choose.click()
        time.sleep(1 + random.random())
        export = self.browser.find_element(by=By.XPATH, value='//span[text()="Export"]')
        export.click()
        time.sleep(1 + random.random())
        output = self.browser.find_element(by=By.XPATH, value='//span[text()="Export citation to text"]')
        output.click()
        time.sleep(5)

    def reset(self):
        self.login()


class Springer(Project):
    def __init__(self, storage='', bgt=True):
        Project.__init__(self, storage, bgt)

    def login(self):
        self.url = 'https://link.springer.com/advanced-search'
        self.browser.get(self.url)
        self.browser.implicitly_wait(5)
        time.sleep(5)
        try:
            accept = self.browser.find_element(by=By.XPATH, value='//span[text()="Cookies"]')
            accept.click()
            time.sleep(1 + random.random())
        except Exception:
            pass

    def set_item(self, entry, item='match'):
        items = ('all', 'exact', 'match', 'without', 'title', 'author', 'editor')
        if item not in items:
            raise ValueError('Unrecognized entry in query')
        else:
            n = items.index(item)
            if n == 0:
                box = self.browser.find_element(by=By.XPATH, value='//*[@id="all-words"]')
            elif n == 1:
                box = self.browser.find_element(by=By.XPATH, value='//*[@id="exact-phrase"]')
            elif n == 2:
                box = self.browser.find_element(by=By.XPATH, value='//*[@id="least-words"]')
            elif n == 3:
                box = self.browser.find_element(by=By.XPATH, value='//*[@id="without-words"]')
            elif n == 4:
                box = self.browser.find_element(by=By.XPATH, value='//*[@id="title-is"]')
            else:
                box = self.browser.find_element(by=By.XPATH, value='//*[@id="author-is"]')
        box.send_keys(entry)
        time.sleep(1 + random.random())

    def query(self):
        search = self.browser.find_element(by=By.XPATH, value='//*[@id="submit-advanced-search"]')
        search.click()
        self.browser.implicitly_wait(5)
        time.sleep(1 + random.random())
        export = self.browser.find_element(by=By.XPATH, value='//*[@id="tool-download"]')
        export.click()
        time.sleep(5)

    def reset(self):
        self.login()


class Wiley(Project):
    def __init__(self, storage='', bgt=True):
        Project.__init__(self, storage, bgt)
        self.entry = ''
        self.point = 1

    def add_query(self, **entry):
        items = ('anywhere', 'title', 'author', 'keyword', 'abstract', 'affiliation')
        maps = ('AllField', 'Title', 'Contrib', 'Keyword', 'Abstract', 'Affiliation')
        if not set(entry.keys()).issubset(set(items)):
            raise ValueError('Unrecognized entry in query')
        for item in entry:
            p = str(self.point)
            content = entry[item]
            sign = maps[items.index(item)]
            query = 'field' + p + '=' + sign + '&text' + p + '=' + content + '&'
            self.entry += query
            self.point += 1

    def query(self):
        self.url = f'https://onlinelibrary.wiley.com/action/doSearch?{self.entry}Ppub='
        self.browser.get(self.url)
        self.browser.implicitly_wait(5)
        time.sleep(1 + random.random())
        export = self.browser.find_element(by=By.XPATH, value='//span[text()="Export Citation(s)"]')
        export.click()
        time.sleep(1 + random.random())
        labels = self.browser.find_elements(by=By.XPATH, value='//div[@class="bulk-content__title"]')
        for label in labels:
            label.click()
            time.sleep(1 + random.random())
        next = self.browser.find_element(by=By.XPATH, value='//button[text()="Next"]')
        next.click()
        time.sleep(1 + random.random())
        result = self.browser.find_element(by=By.XPATH, value='//button[text()="Export"')
        result.click()
        time.sleep(1 + random.random())

    def reset(self):
        self.entry = ''
        self.point = 1


class Taylor(Wiley):
    def __init__(self, storage='', bgt=True):
        Wiley.__init__(self, storage, bgt)

    def login(self, user, key):
        self.browser.get('https://www.tandfonline.com/action/showLogin')
        self.browser.implicitly_wait(5)
        time.sleep(5)
        try:
            accept = self.browser.find_element(by=By.XPATH, value='//a[text()="Accept"]')
            accept.click()
            time.sleep(1 + random.random())
        except Exception:
            pass
        self.username = user
        userbox = self.browser.find_element(by=By.XPATH, value='//*[@id="login"]')
        userbox.send_keys(self.username)
        time.sleep(1 + random.random())
        self.__password = key
        keybox = self.browser.find_element(by=By.XPATH, value='//*[@id="password"]')
        keybox.send_keys(self.__password)
        time.sleep(1 + random.random())
        submit = self.browser.find_element(by=By.XPATH, value='//input[@value="Log in"]')
        submit.click()
        self.browser.implicitly_wait(5)
        time.sleep(1 + random.random())

    def query(self):
        self.url = f'https://www.tandfonline.com/action/doSearch?{self.entry}Ppub='
        self.browser.get(self.url)
        self.browser.implicitly_wait(5)
        time.sleep(1 + random.random())
        export = self.browser.find_element(by=By.XPATH, value='//*[@id="download-search-results"]')
        export.click()
        time.sleep(1 + random.random())
        output = self.browser.find_elements(by=By.XPATH, value='//*[@id="btn-download-search-results"]')[1]
        output.click()
        time.sleep(5)
