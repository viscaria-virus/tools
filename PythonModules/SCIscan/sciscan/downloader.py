from SCIscan.sciscan.scanner import Project
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
import time, random, os


class Task(Project):
    def check(self):
        files = os.listdir(self.path)
        for file in files:
            if file.endswith('.crdownload'):
                self.complete = False
                break
        else:
            self.complete = True
        return self.complete

    def __init__(self, driver, storage, bgt=True, auto=True):
        Project.__init__(self, storage, bgt)
        self.path = storage
        self.auto = auto
        self.start(driver)

    def get_doc(self, doi, source):
        address = f'https://doi.org/{doi}'
        if source == 'SciHub':
            url = f'https://sci-hub.se/{address}'
            self.browser.get(url)
            self.browser.implicitly_wait(5)
            time.sleep(1 + random.random())
            save = self.browser.find_element(by=By.XPATH, value='//*[@id="buttons"]/button')
            save.click()
        elif source == 'CNKI':
            url = 'https://oversea.cnki.net'
            self.browser.get(url)
            self.browser.implicitly_wait(5)
            time.sleep(1 + random.random())
            dropdown = self.browser.find_element(by=By.XPATH, value='//*[@id="DBFieldBox"]/div')
            ActionChains(self.browser).move_to_element(dropdown).perform()
            time.sleep(1 + random.random())
            DOI = self.browser.find_element(by=By.XPATH, value='//li[@value="DOI"]')
            DOI.click()
            time.sleep(1 + random.random())
            box = self.browser.find_element(by=By.XPATH, value='//*[@id="txt_SearchText"]')
            box.send_keys(doi)
            time.sleep(1 + random.random())
            search = self.browser.find_element(by=By.XPATH, value='//*[@id="search"]')
            search.click()
            self.browser.implicitly_wait(5)
            time.sleep(1 + random.random())
            download = self.browser.find_element(by=By.XPATH, value='//td[@class="operat"]/a[@title="Download"]/i')
            download.click()
        else:
            raise ValueError('Unsupported literature resource')
        time.sleep(1 + random.random())

    def get_docs(self, config):
        with iter(open(config, 'r')) as f:
            for line in f:
                item = line.rstrip().split('<<')
                if len(item) == 2:
                    doi = item[0].strip()
                    source = item[1].strip()
                    self.get_doc(doi, source)
        if self.auto == True:
            self.complete = False
            while self.complete == False:
                self.check()
                time.sleep(30)
            self.close()
