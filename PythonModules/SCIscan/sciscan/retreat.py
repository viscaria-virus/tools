import os, csv, xlrd


def extract_doi(src, dir):
    results = []
    if src == 'ScienceDirect':
        for file in os.listdir(dir):
            if file.startswith(src) and file.endswith('.txt'):
                f = iter(open(dir + '/' + file, 'r', encoding='utf-8'))
                for line in f:
                    if 'doi.org' in line:
                        results.append(line[16:].rstrip())
                f.close()
    elif src == 'Wiley':
        f = iter(open(dir + '/pericles_exported_citations.txt', 'r', encoding='utf-8'))
        for line in f:
            if line.startswith('DO  -'):
                result = line[6:].rstrip()
                results.append(result)
        f.close()
    elif src == 'PubMed':
        for file in os.listdir(dir):
            if file.startswith('pubmed-') and file.endswith('-set.txt'):
                f = iter(open(dir + '/' + file, 'r', encoding='utf-8'))
                for line in f:
                    if line.startswith('AID -') and '[doi]' in line:
                        result = line.rstrip()[6:-6]
                        results.append(result)
    elif src == 'Springer':
        csvfile = iter(open(dir + '/SearchResults.csv', 'r'))
        csvfile.readline()
        reader = csv.reader(csvfile)
        results = [row[5] for row in reader]
        csvfile.close()
    elif src == 'Taylor':
        for file in os.listdir(dir):
            if file.startswith('Taylor and Francis search results'):
                csvfile = iter(open(dir + '/' + file, 'r'))
                csvfile.readline()
                reader = csv.reader(csvfile)
                results = [row[-3] for row in reader]
                csvfile.close()
    elif src == 'WOS':
        data = xlrd.open_workbook(dir + '/savedrecs.xls')
        sheet = data.sheet_by_index(0)
        cols = sheet.col(28)[1:] + sheet.col(29)[1:]
        for cell in cols:
            if cell.value != '':
                results.append(cell.value)
    else:
        raise ValueError('Unrecognized/Unsupported source')
    return results


def filter_doi(doi_list):
    return list(set(doi_list))


def extract_record(src, dir):
    records = []
    if src == 'WOS':
        data = xlrd.open_workbook(dir + '/savedrecs.xls')
        sheet = data.sheet_by_index(0)
        for n in range(1, sheet.nrows):
            title = sheet.row(n)[9].value
            doi = sheet.row(n)[28].value
            abstract = sheet.row(n)[34].value
            ID = sheet.row(n)[-2].value
            record = (title, abstract, doi, ID)
            records.append(record)
    elif src == 'PubMed':
        for file in os.listdir(dir):
            if file.startswith('pubmed-') and file.endswith('-set.txt'):
                f = iter(open(dir + '/' + file, 'r', encoding='utf-8'))
                for line in f:
                    if line.startswith('PMID-'):
                        ID = line.rstrip()
                        doi = ''
                    if line.startswith('TI  -'):
                        title = line[6:].rstrip()
                    if line.startswith('AB  -'):
                        abstract = line[6:].rstrip()
                        next = f.readline()
                        while not next[0].isalpha():
                            abstract += next.strip()
                            next = f.readline()
                    if line.startswith('AID -') and '[doi]' in line:
                        doi = line.rstrip()[6:-6]
                    if line.startswith('SO  -'):
                        record = (title, abstract, doi, ID)
                        records.append(record)
                f.close()
    elif src == 'ScienceDirect':
        for file in os.listdir(dir):
            if file.startswith(src) and file.endswith('.txt'):
                f = iter(open(dir + '/' + file, 'r', encoding='utf-8'))
                p = 0
                for line in f:
                    p += 1
                    if p == 2:
                        title = line.rstrip()[:-1]
                    if line.startswith('ISSN'):
                        ID = line.strip()[:-1]
                        doi = ''
                    if 'doi.org' in line:
                        doi = line[16:].rstrip()
                    if line.startswith('Abstract:'):
                        abstract = line[10:].rstrip()
                        next = f.readline().strip()
                        while next != '':
                            abstract += next
                            next = f.readline()
                    if line.strip() == '':
                        record = (title, abstract, doi, ID)
                        records.append(record)
                        p = 0
                f.close()
    elif src == 'Wiley':
        f = iter(open(dir + '/pericles_exported_citations.txt', 'r', encoding='utf-8'))
        f.readline()
        for line in f:
            if line.startswith('TI  -'):
                title = line[6:].rstrip()
            if line.startswith('SN  -'):
                ID = line.rstrip()
                doi = ''
            if line.startswith('DO  -'):
                doi = line[16:].rstrip()
            if line.startswith('AB  -'):
                abstract = line[6:].rstrip()
                next = f.readline().strip()
                while not next.startswith('ER  -'):
                    abstract += next
                    next = f.readline()
            if line.strip() == '':
                record = (title, abstract, doi, ID)
                records.append(record)
        f.close()
    elif src == 'Springer':
        csvfile = iter(open(dir + '/SearchResults.csv', 'r'))
        csvfile.readline()
        reader = csv.reader(csvfile)
        ID = 'None[Springer]'
        for row in reader:
            title = row[0]
            doi = row[5]
            record = (title, '', doi, ID)
            records.append(record)
        csvfile.close()
    elif src == 'Taylor':
        for file in os.listdir(dir):
            if file.startswith('Taylor and Francis search results'):
                csvfile = iter(open(dir + '/' + file, 'r'))
                csvfile.readline()
                reader = csv.reader(csvfile)
                ID = 'None[Taylor]'
                for row in reader:
                    title = row[0]
                    doi = row[-3]
                    record = (title, '', doi, ID)
                    records.append(record)
                csvfile.close()
    else:
        raise ValueError('Unrecognized/Unsupported source')
    return records


def filter_record(records):
    uniques = {}
    others = []
    results = {}
    for record in records:
        doi = record[2].replace('https://doi.org/', '')
        if doi not in uniques and doi != '':
            uniques[doi] = record
        if doi == '':
            others.append(record)
    data = [uniques[doi] for doi in uniques] + others
    for i in range(len(data)):
        results[str(i + 1)] = data[i]
    return results
