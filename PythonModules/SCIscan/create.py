import os


def set_config():
    global period, time, day, driver, storage, browser
    period = input('Set a period for the task: ')
    if period == 'daily':
        day = '0'
    else:
        day = input('Enter an integer to specify a weekday/date: ')
    time = input('Specify a time: ')
    browser = input('Specify a browser type: ')
    driver = input('Specify a webdriver to call: ')
    storage = input('Specify a storage path for the task: ')


def set_account():
    global user, key
    print('Set your WOS account: ')
    user = "'" + input('Enter a username: ') + "'"
    key = "'" + input('Enter the password: ') + "'"


def generate(query, project):
    entries = {}
    db = ('WOS', 'PubMed')
    with iter(open(query, 'r')) as f:
        for line in f:
            if line.startswith('<<</'):
                entries[task] = entry
            elif '=' in line:
                item = line.strip().replace('\n', '').replace('\r', '')
                entry.append(item)
            elif line.startswith('>>>'):
                task = line[3:-1]
                entry = []
            else:
                continue
    os.mkdir(storage + '\\' + project)
    os.chdir(storage + '\\' + project)
    for task in entries:
        with iter(open(task + '.py', 'w')) as f:
            f.write('#{' + period + '}(' + time + ')[' + day + ']\n')
            f.write('from SCIscan.sciscan.scanner import *\n')
            f.write('task=Project(storage=\'' + storage + '\',bgt=False,tool=\'' + browser + '\')\n')
            f.write('task.start(\'' + driver + '\')\ntask0=' + db[0] + '(task)\ntask1=' + db[1] + '(task)\n')
            for i in range(2):
                if i == 0:
                    f.write('task0.login(' + user + ',' + key + ')\n')
                else:
                    f.write('task' + str(i) + '.login()\n')
                for item in entries[task]:
                    sign = item.split('=')[0]
                    value = item.split('=')[1]
                    if i == 1:
                        if sign == 'abstract':
                            key = 'Title/Abstract'
                        else:
                            key = sign.title()
                        item = key + '=' + value
                        key = sign.lower()
                    f.write('task' + str(i) + '.add_query(' + item + ')\n')
                f.write('try:\n    task' + str(i) + '.query()\nexcept Exception:\n')
                f.write('    f=open(\'log.txt\',\'a\')\n')
                f.write('    f.write(\'' + task + '>' + db[i] + '-Query No Result.' + r'\n' + '\')\n')
                f.write('    f.close()\n')
            f.write('task.close()\nprint(\'Scan Finish.\')\n')


if __name__ == '__main__':
    set_account()
    set_config()
    filein = input('Enter a query configuration file: ')
    dirout = input('Name the project: ')
    generate(filein, dirout)
