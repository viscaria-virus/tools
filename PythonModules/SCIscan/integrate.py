from SCIscan.sciscan.retreat import extract_record, filter_record
import xlsxwriter


def extract(src, storage):
    try:
        record = extract_record(src, storage)
        return record
    except Exception:
        print(src + ' no result.')


def integrate(storage):
    records0 = extract('WOS', storage)
    records1 = extract('PubMed', storage)
    records2 = extract('ScienceDirect', storage)
    records3 = extract('Wiley', storage)
    records4 = extract('Springer', storage)
    records5 = extract('Taylor', storage)
    total = []
    for i in range(6):
        result = eval('records' + str(i))
        if result != None:
            total += result
    results = filter_record(total)
    return results


def export(results, outfile):
    wb = xlsxwriter.Workbook(outfile + '.xlsx')
    ws = wb.add_worksheet('SCIscan')
    ws.write_row(0, 0, ['', 'Title', 'Abstract', 'DOI', 'ID'])
    p = 1
    for order in results:
        record = results[order]
        ws.write(p, 0, order)
        for i in range(len(record)):
            ws.write(p, i + 1, record[i])
        p += 1
    wb.close()


if __name__ == '__main__':
    path = input('Enter the storage path of the result files: ')
    out = input('Enter the out file name: ')
    results = integrate(path)
    export(results, out)
    print('Integerate Complete.')
