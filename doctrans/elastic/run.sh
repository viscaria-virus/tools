#!/bin/bash
docin(){
    echo "python3 trans.py $1 &&" >> $2
}
quiet(){
    echo "sleep 2" >> $1
}
initsh(){
    echo '#!/bin/bash' > $1
}
read -p "Input the number of documents to be translated: " n
i=0
initsh .work.sh
while (( $i < $n ))
do
    ((i++))
    read -p "Input the document $i: " doc
    sed -i "s/￾//g" $doc
    mv $doc ${doc}.src
    cat ${doc}.src|tr '\r\n' ' '|tr '\n' ' '>$doc
    rm ${doc}.src
    docsize=$(wc -c < $doc)
    if (( $docsize <= 6000 ))
    then
        docin $doc .work.sh
        quiet .work.sh
    else
        count=$(echo "$docsize/6000"|bc)
        rem=$(echo "$docsize%6000"|bc)
        if (( $rem == 0 ))
        then
            count=$(echo "$count-1"|bc)
        fi
        split -d -b 6000 $doc .${doc}_
        m=0
        while (( $m <= $count ))
        do
            if (( $m < 10 ))
            then
                docin .${doc}_0$m .${doc}.trans
                echo "cat .${doc}_0${m}_trans.doc >> ${doc}_trans.doc &&" >> .${doc}.trans
                quiet .${doc}.trans
            else
                docin .${doc}_$m .${doc}.trans
                echo "cat .${doc}_${m}_trans.doc >> ${doc}_trans.doc &&" >> .${doc}.trans
                quiet .${doc}.trans
            fi
            ((m++))
        done
        echo "rm .${doc}_*" >> .${doc}.trans
        echo 'rm $0' >> .${doc}.trans
        chmod a+x .${doc}.trans    
        echo "./.${doc}.trans &&" >> .work.sh
    fi
done
echo 'echo -e "\033[32m Translation Over \033[0m"' >> .work.sh
echo 'rm $0' >> .work.sh
chmod a+x .work.sh
./.work.sh &
