import pdfplumber, sys

filein = sys.argv[1]
fileout = sys.argv[2]
f = pdfplumber.open(filein)
with open(file=fileout, mode='a', encoding='utf-8') as g:
    for page in f.pages:
        text = page.extract_text()
        g.write(text)
f.close()
