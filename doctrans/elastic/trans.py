import requests, random, sys
from hashlib import md5
from API_info import appid, appkey

from_lang = 'auto'
to_lang = 'zh'
url = 'http://api.fanyi.baidu.com/api/trans/vip/translate'
if len(sys.argv) == 1:
    document = input('Input the document to be translated: ')
else:
    document = sys.argv[1]
with open(str(document), 'r') as f:
    query = f.read()


# Generate salt and sign
def make_md5(s, encoding='utf-8'):
    return md5(s.encode(encoding)).hexdigest()


salt = random.randint(32768, 65536)
sign = make_md5(appid + query + str(salt) + appkey)
# Build request
headers = {'Content-Type': 'application/x-www-form-urlencoded'}
payload = {'appid': appid, 'q': query, 'from': from_lang, 'to': to_lang, 'salt': salt, 'sign': sign}
# Send request and receive response
r = requests.post(url, params=payload, headers=headers)
response = r.json()
# Save response
result = response['trans_result']
lines = len(result)
for line in range(lines):
    with open(document + '_trans.doc', 'a') as ft:
        ft.write(result[line]['dst'] + '\n')
