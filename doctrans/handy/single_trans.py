import pdfplumber, sys, requests, random, os, time
from hashlib import md5
from API_info import appid, appkey

filein = sys.argv[1]
texts = ''
fileout = filein.replace('.pdf', '')
with pdfplumber.open(filein) as g:
    for page in g.pages:
        text = page.extract_text().replace('\r\n', '') \
            .replace('\n', '').replace('￾', '') \
            .replace('.', '.\n')
        texts += text
tail = 'reference'
tail1 = texts.rfind(tail)
tail2 = texts.rfind(tail.upper())
tail3 = texts.rfind(tail.capitalize())
label = max(tail1, tail2, tail3)
if label > 0:
    end = label
else:
    end = len(texts)
head = 'abstract'
head1 = texts.find(head)
head2 = texts.find(head.upper())
head3 = texts.find(head.capitalize())
tag = min(head1, head2, head3)
if tag > 0:
    start = tag
else:
    start = 0
text = texts[start:end]
f = iter(open(file=fileout, mode='a+', encoding='utf-8'))
f.write(text)
f.seek(0, 0)
content = ''
i = 0
for line in f:
    content += line.replace('\n', '')
    if len(content) > 5000:
        content += '\n'
        content = content.replace(line, '')
        part = fileout + '_part_' + str(i)
        with open(file=part, mode='w', encoding='utf-8') as h:
            h.write(content)
        content = ''
        i += 1
if content != '':
    with open(file=fileout + '_part_' + str(i), mode='w', encoding='utf-8') as h:
        h.write(content)
f.close()
url = 'http://api.fanyi.baidu.com/api/trans/vip/translate'


def make_md5(s, encoding='utf-8'):
    return md5(s.encode(encoding)).hexdigest()


headers = {'Content-Type': 'application/x-www-form-urlencoded'}
payload = {'appid': appid, 'from': 'auto', 'to': 'zh'}
f = iter(open(file=fileout + '_trans.doc', mode='a', encoding='utf-8'))
for j in range(i + 1):
    text2trans = fileout + '_part_' + str(j)
    with open(file=text2trans, mode='r', encoding='utf-8') as g:
        query = g.read()
    salt = random.randint(32768, 65536)
    sign = make_md5(appid + query + str(salt) + appkey)
    payload['q'] = query
    payload['salt'] = salt
    payload['sign'] = sign
    os.remove(text2trans)
    response = requests.post(url, params=payload, headers=headers)
    gap = random.random() + random.randint(2, 3)
    time.sleep(gap)
    result = response.json()['trans_result']
    lines = len(result)
    for line in range(lines):
        f.write(result[line]['dst'] + '\n')
f.close()
os.remove(fileout)
